/* 
 * tps65217-irq.c  --  TI TPS65217 
 * 
 * Copyright 2010 Texas Instruments Inc. 
 * Copyright 2012 Omni-ID USA 
 * 
 * Author: Andrew Bradford <andrew....@omni-id.com> 
 * 
 * Modified from tps65910-irq.c 
 * 
 * This program is free software; you can redistribute it and/or modify it 
 * under  the terms of the GNU General  Public License as published by the 
 * Free Software Foundation;  either version 2 of the License, or (at your 
 * option) any later version. 
 * 
 */ 
 
#include <linux/kernel.h> 
#include <linux/module.h> 
#include <linux/init.h> 
#include <linux/device.h> 
#include <linux/interrupt.h> 
#include <linux/irq.h> 
#include <linux/mfd/tps65217.h> 

#include <linux/reboot.h>
 
#define DIRECT_RESET 1

static irqreturn_t tps65217_irq(int irq, void *irq_data) 
{ 
        struct tps65217 *tps65217 = irq_data; 
        unsigned int int_reg, status_reg; 
 
        tps65217_reg_read(tps65217, TPS65217_REG_INT, &int_reg); 
        tps65217_reg_read(tps65217, TPS65217_REG_STATUS, &status_reg); 
        if (status_reg) 
                dev_dbg(tps65217->dev, "status now: 0x%X\n", status_reg); 
 
        if (!int_reg) 
                return IRQ_NONE; 
 
        if (int_reg & TPS65217_INT_PBI) { 
                /* Handle push button */ 
                dev_dbg(tps65217->dev, "power button status change\n"); 

		if (DIRECT_RESET) {
			kernel_restart("reboot");
		} else {
            		input_report_key(tps65217->pwr_but, KEY_POWER, 
                                status_reg & TPS65217_STATUS_PB); 
            		input_sync(tps65217->pwr_but); 
		}
        } 
        if (int_reg & TPS65217_INT_ACI) { 
                /* Handle AC power status change */ 
                dev_dbg(tps65217->dev, "AC power status change\n"); 
                /* Press KEY_POWER when AC not present */ 
                input_report_key(tps65217->pwr_but, KEY_POWER, 
                                ~status_reg & TPS65217_STATUS_ACPWR); 
                input_sync(tps65217->pwr_but); 
        } 
        if (int_reg & TPS65217_INT_USBI) { 
                /* Handle USB power status change */ 
                dev_dbg(tps65217->dev, "USB power status change\n"); 
        } 
 
        return IRQ_HANDLED; 
} 
 
int tps65217_irq_init(struct tps65217 *tps65217, 
                struct tps65217_irq_data *pdata) 
{ 
        int ret; 
        int flags = IRQF_ONESHOT; 
 
        tps65217->pwr_but = input_allocate_device(); 
        if (!tps65217->pwr_but) { 
                dev_err(tps65217->dev, 
                        "Failed to allocated pwr_but input device\n"); 
                ret = -ENOMEM; 
        } 
        tps65217->pwr_but->evbit[0] = BIT_MASK(EV_KEY); 
        tps65217->pwr_but->keybit[BIT_WORD(KEY_POWER)] = BIT_MASK(KEY_POWER); 
        tps65217->pwr_but->name = "tps65217_pwr_but"; 
        ret = input_register_device(tps65217->pwr_but); 
        if (ret) { 
                dev_err(tps65217->dev, "Failed to register button device\n"); 
                goto err_out; 
        } 
 
        tps65217->irq = pdata->irq_num; 
        ret = request_threaded_irq(tps65217->irq, NULL, tps65217_irq, flags, 
                                   "tps65217", tps65217); 
        if (ret != 0) { 
                dev_err(tps65217->dev, "Failed to request IRQ: %d\n", ret); 
                goto err_out; 
        } 
        irq_set_irq_type(tps65217->irq, IRQ_TYPE_LEVEL_LOW); 
 
        return ret; 
 
err_out: 
        input_unregister_device(tps65217->pwr_but); 
        return ret; 
} 
 
int tps65217_irq_exit(struct tps65217 *tps65217) 
{ 
        input_unregister_device(tps65217->pwr_but); 
        free_irq(tps65217->irq, tps65217); 
        return 0; 
} 
